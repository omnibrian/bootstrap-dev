# Bootstrap Development Environment

## Instructions

OS X:

1. Install Apple command line tools: `xcode-select --install`
1. Install `pip`: `curl https://bootstrap.pypa.io/get-pip.py | sudo python`
1. Install `ansible`: `sudo pip install ansible`
1. Run `ansible-playbook main.yml -i inventory -K` from the root of this repo

